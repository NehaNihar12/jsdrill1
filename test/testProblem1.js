const data = require("../input.js");
const problem1 = require("../problem1.js")
//test problem 1

let testCases = {
    1:problem1(data,33),
    2:problem1([],33),
    3:problem1(33),
    4:problem1('abc'),
    5:problem1(data),
    6:problem1({"id":33,"car_make":"Jeep","car_model":"Wrangler","car_year":2011},33),
    7:problem1({})

}

for(let testCase in testCases){
    //console.log(testCases[testCase])
    if([]){
        console.log(testCases[testCase])
    }else{
        let result = testCases[testCase]
        console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`)
    }
}
