const data = require("../input.js");
const problem5 = require("../problem5.js")
//1.
let result = problem5(data,2000)
console.log(result.length)
//2.null car input
 result = problem5(2000)
console.log(result)
//3.null year
 result = problem5(data)
console.log(result)
//4.not an object
 result = problem5('abc',2000)
console.log(result)
//5.not an integer
 result = problem5(data,-2000)
console.log(result)