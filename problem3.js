//sort car models
function sortCarModels(cars){
    
    if(typeof cars !=='object' || typeof cars==='null'|| cars.length ===0){
        return [];
    }
    let carModels = []
    //add car models to an array
    for(let i=0;i<cars.length;i++){
        carModels.push(cars[i].car_model.toLowerCase());
    }
    
    return carModels.sort();
}


module.exports = sortCarModels;