//function to return the details of a car by its carid
function returnCarDetailById(cars,carId){
    if(typeof cars === 'null'||typeof carId==='null'||typeof cars!== 'object'||cars.length===0){
        return [];
    }
    if(!Array.isArray(cars)){
        if(cars.hasOwnProperty('id') && cars.id ===carId)
            return cars;
        else
            return [];    
    }
    for(let i =0;i<cars.length;i++){
        if(cars[i].id===carId){
            return cars[i]
        }
    }
    return []
}
module.exports = returnCarDetailById
