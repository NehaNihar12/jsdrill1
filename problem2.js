
//function to return the data of last Car 

function infoLastCar(cars){
    
    if(typeof cars !=='object' || typeof cars ==='null' ||cars.length===0){
        return []
    }

    return cars[cars.length-1];
}

module.exports = infoLastCar;
